package com.test.app.algos;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Test class for understanding - open for rewrite
 */
public class TestGraphKnowledge {
    private AdjacencyListGraphImpl graph;
    private Boolean[] isMarked;
    private Integer[] edgeTo;

    public TestGraphKnowledge(AdjacencyListGraphImpl graph){
        this.graph = graph;
        this.isMarked = new Boolean[graph.size()];
        for(int i = 0; i< isMarked.length; i++){
            isMarked[i] = false;
        }
        this.edgeTo = new Integer[graph.size()];
    }


    public void initializeDFS(int V){
       Iterable<Integer> itr =  graph.adj(V);
       isMarked[V] = true;

       for(Integer adjVertex  : itr){
           if(!isMarked[adjVertex]){
               initializeDFS(adjVertex);
               edgeTo[adjVertex] = V;
           }
       }
    }

    public Iterable<Integer> traverseDFS(int V, int S){
        List<Integer> deque = new LinkedList<>();

        for(int i=V; i!= S;i=edgeTo[i]){
            System.out.println(i);
            deque.add(i);
        }

        return deque;
    }

    public void initializeBFS(int V){
        Queue<Integer> queue = new LinkedList<>();
        queue.add(V);

        while(!queue.isEmpty()){
            Integer vertex = queue.remove();
            isMarked[vertex] = true;
            Iterable<Integer> adjVert= graph.adj(vertex);
            for(Integer vert: adjVert){
                if(!isMarked[vert]){
                    //isMarked[vert] = true;
                    queue.add(vert);
                    edgeTo[vert] = vertex;
                }
            }
        }
    }

    public Iterable<Integer> traverseBFS(int v, int s){
        if(edgeTo[v]==null) new LinkedList<>();
        List<Integer> connectionList = new LinkedList<>();

        for(int i=v; i!=s; i=edgeTo[i]){
            connectionList.add(i);
        }
        return connectionList;
    }

}
