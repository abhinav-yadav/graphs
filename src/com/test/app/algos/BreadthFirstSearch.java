package com.test.app.algos;

import java.util.LinkedList;
import java.util.List;

/**
 * Iterative graph traversal
 */
public class BreadthFirstSearch {
    private Boolean[] isMarked;
    private Integer[] edgeTo;

    private Queue queue = new Queue();

    public BreadthFirstSearch(AdjacencyListGraphImpl graphImpl, int V){
        //Initialize the marked array
        isMarked = new Boolean[graphImpl.size()];
        edgeTo = new Integer[graphImpl.size()];

        for(int i = 0; i< isMarked.length; i++){
            isMarked[i] = false;
        }

        //Now travers the tree
        bfs(graphImpl, V);
    }

    private void bfs(AdjacencyListGraphImpl graphImpl, int V){
        isMarked[V] = true;
        queue.push(V);

        while(!queue.isEmpty()){
            Integer vertex = queue.pop();
            isMarked[vertex] = true;
            Iterable<Integer> adjacentVerticies = graphImpl.adj(vertex);
            for(Integer adjVertex: adjacentVerticies){
                if(isMarked[adjVertex]) continue;
                System.out.println("BFS vertex found: "+adjVertex);
                queue.push(adjVertex);
                edgeTo[adjVertex] = vertex;
                //isMarked[adjVertex] = true;
            }
        }
    }

    public Iterable<Integer> traverseBFS(int v, int s){
        if(edgeTo[v]==null) new LinkedList<>();
        List<Integer> connectionList = new LinkedList<>();

        for(int i=v; i!=s; i=edgeTo[i]){
            connectionList.add(i);
        }
        return connectionList;
    }

}
