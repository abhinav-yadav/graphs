package com.test.app.algos;

/**
 * This is a Linked List implementation for a Queue
 */
public class Queue {
    /**
     * Top most & last node of the queue
     */
    Node head, tail;

    /**
     * Add to the top of queue
     * @param vertex
     */
    public void push(Integer vertex){
        Node newNode = new Node(vertex);
        if(isEmpty()) { // first node
            head = newNode;
            tail = head;
        } else {
            Node oldHead = head;
            oldHead.previous = newNode;
            head = newNode;
        }
    }

    /**
     * Remove the remove from top of queue
     * @return
     */
    public Integer pop(){
        if(isEmpty()) return null; //last node gone
        Node oldTail = tail;
        Node nextTail = tail.previous;
        tail = nextTail;
        return oldTail.vertex;
    }

    public Boolean isEmpty(){
        return tail==null;
    }

    /**
     * private class to represent the node object
     * Node stores its own data and reference to previous node
     */
    private class Node{
        Integer vertex;
        Node previous;

        public Node(Integer vertex){
            this.vertex = vertex;
        }

        public String toString(){
            return this.vertex.toString();
        }
    }
}

