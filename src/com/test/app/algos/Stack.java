package com.test.app.algos;

import java.util.Iterator;

public class Stack implements Iterable<Integer>{
    private Integer[] arr = new Integer[10];
    private int size = 0;

    public void push(Integer newElement){
        arr[size++] = newElement;
    }

    public Integer pop(){
        if(size==0) return null;
        return arr[--size];
    }

    public Boolean isEmpty(){
        return size == 0;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new StackIterator();
    }

    public class StackIterator implements Iterator<Integer>{
        private int pointer = 0;

        @Override
        public boolean hasNext() {
            return arr[pointer]!=0;
        }

        @Override
        public Integer next() {
            return arr[pointer++];
        }
    }
}