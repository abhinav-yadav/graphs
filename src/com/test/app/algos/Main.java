package com.test.app.algos;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {
        /**
         * Test Implementation
         */
        /*
        //Construct the graph
        AdjacencyListGraphImpl graphImpl = new AdjacencyListGraphImpl(6);
        //Initialize the graph
        graphImpl.addEdge(0,1);
        graphImpl.addEdge(1,2);
        graphImpl.addEdge(2,3);
        graphImpl.addEdge(3,4);
        graphImpl.addEdge(3,5);
        //Now check it picks up correct adjacent nodes
        System.out.println("Expected [2, 4, 5]: is"+graphImpl.adj(3));*/


        //Construct the graph
        AdjacencyListGraphImpl graphImpl = new AdjacencyListGraphImpl(6);
        //Initialize the graph
        graphImpl.addEdge(0,1);
        graphImpl.addEdge(1,2);
        graphImpl.addEdge(2,3);
        graphImpl.addEdge(3,5);
        graphImpl.addEdge(3,4);
        graphImpl.addEdge(5,2);//Connected back, tricky graph condition

        /*System.out.println("*** DFS Implementation ***");
        // No vertex should be printed twice
        DepthFirstSearch dfs = new DepthFirstSearch(graphImpl, 0);
        // Get path for 5 from 0
        Iterator<Integer> itr = dfs.getPathTo(5, 0).iterator();
        while(itr.hasNext()){
            System.out.println("Stack Value: "+itr.next());
        }

        System.out.println("*** BFS Implementation ***");
        // No vertex should be printed twice
        BreadthFirstSearch bfs = new BreadthFirstSearch(graphImpl, 0);*/

        /** Test Implementation **/
        TestGraphKnowledge testGraphKnowledge = new TestGraphKnowledge(graphImpl);
        /*testGraphKnowledge.initializeDFS(0);

        Iterator<Integer> itr = testGraphKnowledge.traverseDFS(5, 3).iterator();
        while(itr.hasNext()){
            System.out.println("Stack Value: "+itr.next());
        }*/

        testGraphKnowledge.initializeBFS(0);

        Iterator<Integer> itr = testGraphKnowledge.traverseBFS(5, 0).iterator();
        while(itr.hasNext()){
            System.out.println("Stack Value: "+itr.next());
        }



    }
}
