package com.test.app.algos;

import java.util.ArrayList;
import java.util.List;

/**
 * Each vertex here is able to keep track of its connected vertices
 */
public class AdjacencyListGraphImpl {

    List<Integer>[] graph;

    AdjacencyListGraphImpl(int V){
        graph = (List<Integer>[]) new ArrayList[V];
        for(int i = 0; i< V ;i ++){
            graph[i] = new ArrayList<>();
        }
    }

    public void addEdge(int v, int w){
        graph[v].add(w);
        graph[w].add(v);
    }

    public Iterable<Integer> adj(int v){
        return graph[v];
    }

    public Integer size(){
        return graph.length;
    }
}
