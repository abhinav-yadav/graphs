package com.test.app.algos;

/**
 * Given a graph, this algorithm is looking to traverse from a vertex
 * to all paths from it. RECURSIVE
 */
public class DepthFirstSearch {
    private boolean[] isMarked;
    private Integer[] edgeTo;

    public DepthFirstSearch(AdjacencyListGraphImpl graphImpl, int V){

        //Initialize the marked array
        isMarked = new boolean[graphImpl.size()];
        edgeTo = new Integer[graphImpl.size()];

        for(int i = 0; i< isMarked.length; i++){
            isMarked[i] = false;
        }

        //Traverse recursively to find the vertices connected to V
        depthFirstSearch(graphImpl, V);
    }

    private void depthFirstSearch(AdjacencyListGraphImpl graph, Integer v){
        isMarked[v] = true;
        Iterable<Integer> adjacentVertices = graph.adj(v);
        for(Integer connectedVertex: adjacentVertices){
            if(!isMarked[connectedVertex]) {
                //System.out.println("DFS vertex found: "+connectedVertex +" for V:" +v);
                depthFirstSearch(graph, connectedVertex);
                edgeTo[connectedVertex] = v;
            }
        }
    }

    /**
     * Get path from source s to any vertex on the graph
     * @param v
     * @param s
     * @return
     */
    public Iterable<Integer> getPathTo(Integer v, Integer s){
        if(!hasPathTo(v)) return null;
        Stack stack = new Stack();
        for(int i = v; i != s; i = edgeTo[i]){
            stack.push(i);
        }
        stack.push(s);
        return stack;
    }

    public void printAllEdges(Integer... vertecies){
        for(int i :  vertecies){
            System.out.println("Connected vertex: "+i+" to: "+ edgeTo[i]);
        }
    }

    private boolean hasPathTo(Integer v){
        return isMarked[v];
    }


}
